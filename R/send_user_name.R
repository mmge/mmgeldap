#'Send email to user with username
#'
#'Send an email reminding a user of their username.
#'
#'@param user The output of the \code{get_ldap_user()} function
#'@param uid The uid of the user whose username is requested
#'@param new Is the user a new user?
#'
#'@return \code{TRUE} if email is sent, otherwise \code{FALSE}
#'
#'@export
send_user_name <- function(user, uid, new = FALSE, extra_bcc = NULL) {

  bcc <- c("biobank@iu.edu", extra_bcc)

  if(missing(user)) {
    user <- get_ldap_user(uid)
  }

  if(new) {

    sbj <- "Welcome to the IUGB system. Here is your new username."

    msg <- glue::glue("<p>Greetings {user$title} {user$last_name},</p>",
                      "<p>This is an automatically generated email to inform you ",
                      "that you have been granted access to the IU Genetics ",
                      "Biobank (IUGB) system. This email contains your unique ",
                      "username that can be used to access applications such as ",
                      "catalogs of biospecimen collections hosted at IUGB. You ",
                      "should receive a separate email with a randomly-generated ",
                      "password. If you haven't received the password email within ",
                      "an hour of receiving this email, please contact IUGB staff ",
                      "or your biorepository program manager for assistance.\n\n</p>",
                      "<p>Your username is:\n\n\t{user$uid}\n\n</p>",
                      "<p>If you have any issues please contact your biorepository ",
                      "program manager.\n\n</p>",
                      "<p>Thanks,\n\n</p>",
                      "<p>The IU Genetics Biobank Team\n\n</p>")

  } else {

    sbj <- "IUGB Username request"

    msg <- glue::glue("<p>Greetings {user$title} {user$last_name},</p>",
                      "<p>This email contains the username that you can ",
                      "use to access applications on the IUGB system to which you ",
                      "have been granted access.</p>",
                      "<p>Your username is:\n\n\t{user$uid}</p>",
                      "<p>If you have any issues please contact your biorepository ",
                      "program manager.</p>",
                      "<p>Thanks,</p>",
                      "<p>The IU Genetics Biobank Team</p>")
  }

  x <- try(mmge::send_email(subject = sbj, msg, to = user$email, bcc = bcc,
                   account_name = "biobank", html = TRUE))

  return(!inherits(x, "try-error"))

}