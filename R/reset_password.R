#'Reset a User Password
#'
#'Sets a user's password to a new randomly generated one. Optionally sends
#'an email informing them of the new password with a link to change it.
#'
#'@param uid The uid of the user whose password is to be reset
#'@param email Should an email be sent to the user?
#'@param new Is the user a new user?
#'
#'@return \code{TRUE} if update is successful, otherwise \code{FALSE}
#'
#'@export
reset_password <- function(uid, email = TRUE, new = FALSE, extra_bcc = NULL) {

  bcc = "biobank@iu.edu"

  user <- get_ldap_user(uid)

  pw <- random_password()

  url <- sprintf("https://apps.medgen.iupui.edu/account_management?pw=%s", pw)

  pw_update <- set_user_password(user$uid, pw)

  if(pw_update & email) {

    if(new) {

      sbj <- "Welcome to the IUGB system. Here is your temporary password."

      msg <- glue::glue("Greetings {user$title} {user$last_name},\n\n",
                        "You have been added to the IU Genetics Biobank (IUGB) system. ",
                        "This email contains a randomly-generated password that you can ",
                        "use to access applications on the IUGB system to which you ",
                        "have been granted access. While not required, it is recommended ",
                        "that you use the link below to change the password to something ",
                        "more memorable.\n\n",
                        "You should have received a seperate email with your ",
                        "username. If you haven't received the username email within ",
                        "an hour please contact IUGB staff or your biorepository ",
                        "program manager for assistance.\n\n",
                        "Your temporary password is:\n\n\t{pw}\n\n",
                        "Please visit:\n\n\t{url}\n\n",
                        "to update this password to something more memorable. If ",
                        "you have any issues please contact your biorepository ",
                        "project manager.\n\n",
                        "Thanks,\n\n",
                        "The IU Genetics Biobank Team")

      if(email) {
        send_user_name(user, new = TRUE, extra_bcc = extra_bcc)
      }

    } else {

      sbj <- "IUGB Password Reset"

      msg <- glue::glue("Greetings {user$title} {user$last_name}, \n\n",
                        "Your password for the IU Genetics Biobank (IUGB) system ",
                        "has been reset.\n\n",
                        "Your new randomly-generated password is:\n\n\t{pw}\n\n",
                        "Please visit:\n\n\t{url}\n\nto update this password ",
                        "to something more memorable. If you have any issues ",
                        "please contact your biorepository program manager.\n\nThanks,\n\n",
                        "The IU Genetics Biobank Team")

    }

    mmge::send_email(subject = sbj, msg, to = user$email, bcc = bcc,
                     account_name = "biobank", html = FALSE)

  } else {
    if(!pw_update) {
      warning("Password was not updated")
    }
  }

  return(invisible(pw_update))

}