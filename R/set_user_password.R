#' Change a User Password
#'
#' Changes a users password in the LDAP system
#'
#' @param uid The uid of the user whose password is to be changed
#' @param pw The password to apply to the user
#'
#' @return \code{TRUE} if the update is successful, otherwise \code{FALSE}
#'
#' @export
set_user_password <- function(uid, password) {

  user <- build_user_dn(uid)

  x <- build_ldap_query(type = "passwd", s = password, user)

  x <- build_ldap_command(x)

  result <- execute_ldap_command(x)

  return(invisible(length(attributes(result)) == 0))

}
