remove_user_memberships <- function(uid, groups) {

  combine_ldifs(.dots = sapply(groups, function(g) {

    ldif_modify(
      dn = g,
      delete = list(memberuid = uid)
    )

  }))

}

add_user_memberships <- function(uid, groups) {

  combine_ldifs(.dots = sapply(groups, function(g) {

    ldif_modify(
      dn = g,
      add = list(memberuid = uid)
    )

  }))

}
